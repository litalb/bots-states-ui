Please notice!

if you are not running the Bots server on localhost, on port 8081 - you should update the server address in Config.js file.

## Available Scripts

In the project directory, you can run:

### `npm install`

This command installs all the needed packages. You should run it before running "npm start"

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!