import React, {Component} from 'react';
import {configuration} from "./Config";
import './App.css';

const Status = {
  FREE: "free",
  BUSY: "busy",
};

const supportedBotsNames = ["customersupbot1", "customersupbot2", "customersupbot3"];

class App extends Component{

    constructor(props){
        super(props);

        this.state ={
            bots: [],
        };

        this.updateBotsState();
    }

    componentDidMount() {
        this.interval = setInterval(() =>
            this.updateBotsState(), 5000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateBotsState(){
        const server = configuration.serverAddress;
        fetch(`${server}/getBotsInfo`)
            .then(response => response.json())
            .then(bots => bots.filter(bot => supportedBotsNames.includes(bot.name)))
            .then(data => this.setState({bots: data}));
    }

    render() {
        const {bots} = this.state;

        return (
            <div className="App">
                <div className="bots-states-grid">
                    {bots.map(bot =>
                        <React.Fragment key={bot._id}>
                            <BotName  name={bot.name}/>
                            <BotStatus status={bot.status}/>
                        </React.Fragment>
                    )}
                </div>
            </div>
        );
    }
}

function BotName(props){
  const {name} = props;
  return (
      <div className="bot-name">
        {name}
      </div>
  );
}

function BotStatus(props) {
  const {status} = props;
  return (
      <div className={"bot-status " + (status=== Status.FREE? "free-status": "busy-status")}>
        {status}
      </div>
  );
}

export default App;
